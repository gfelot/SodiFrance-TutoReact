import React from 'react';

const GifCardLoading = () => {

    return (
        <div className="card">
            <div className="card-image">
                <figure className="image is-16by9">
                    <img src="http://via.placeholder.com/640x360?text=Loading..." alt="Loading" />
                </figure>
            </div>
            
            <div className="card-content">
                <div className="media">
                    <div className="media-content">
                        <p className="title is-4">Loading Title...</p>
                    </div>
                </div>

                <div className="content">
                    URL : <a>Coming Soon</a>
                </div>
            </div>
        </div>
    );
}

export default GifCardLoading;