import React from 'react'

const GifCard = (props) => {
	// console.log('Props in Card', props.data)

	const { url, title, image_original_url } = props.data

	return (
		<div className="card">
			<div className="card-image">
				<figure className="image is-16by9">
					<img src={image_original_url} alt="Placeholder" />
				</figure>
			</div>

			<div className="card-content">
				<div className="media">
					<div className="media-content">
						<p className="title is-4">{title}</p>
					</div>
				</div>

				<div className="content">
					URL :
					<a href={url} target="_blank">
						{url}
					</a>
				</div>
			</div>
		</div>
	)
}

export default GifCard
