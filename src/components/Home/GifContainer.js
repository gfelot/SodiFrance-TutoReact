import React, { Component } from 'react'
import { connect } from 'react-redux'
import GifCard from './GifCard'
import GifCardLoading from './GifCardLoading'
import GifCardError from './GifCardError'
import { fetchData } from '../../actions/fetchData'

class GifContainer extends Component {
  componentWillMount() {
    this.props.fetchData(this.props.tags)
  }

  // componentDidMount() {
  //     console.log('Props Container', this.props)
  // }

  // Conditional rendering
  // if ... return...
  render() {
    if (this.props.loading) {
      return (
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <GifCardLoading />
          </div>
        </div>
      )
    } else if (this.props.hasError) {
      return (
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <GifCardError />
          </div>
        </div>
      )
    } else {
      return (
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <GifCard data={this.props.data} />
          </div>
        </div>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    ...state.giphy
    // loading: state.giphy.loading,
    // hasError: state.giphy.hasError,
    // data: state.giphy.data
  }
}

export default connect(mapStateToProps, { fetchData })(GifContainer)
