import React from 'react'
import GifContainer from './GifContainer'
import SearchBar from './SearchBar'

const Home = () => {
	return (
		<div>
			<SearchBar />
			<GifContainer />
		</div>
	);
};

export default Home;