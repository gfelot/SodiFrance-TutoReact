import React from 'react'

const GifCard = () => {

	return (
		<div className="card">
			<div className="card-image">
				<figure className="image is-16by9">
					<img src="http://via.placeholder.com/640x360/e74c3c/34495e?text=ERROR" alt="ERROR" />
				</figure>
			</div>
            
			<div className="card-content">
				<div className="media">
					<div className="media-content">
						<p className="title is-4">ERROR</p>
					</div>
				</div>

				<div className="content">
                    URL : <a>ERROR ERROR ERROR</a>
				</div>
			</div>
		</div>
	)
}

export default GifCard