import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchData } from '../../actions/fetchData'

class SearchBar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tags: ''
    }
  }

  _onChangeInput = e => {
    this.setState({ tags: e.target.value })
  }

  _handleKeyPress = e => {
    if (e.key === 'Enter') {
      const tags = this.state.tags
      if (tags === '') {
        this.props.fetchData('dog')
      } else {
        this.props.fetchData(this.state.tags)
      }
    }
  }

  _buttonClicked = () => {
    const tags = this.state.tags
    if (tags === '') {
      this.props.fetchData('dog')
    } else {
      this.props.fetchData(this.state.tags)
    }
  }

  render() {
    return (
      <section className="section">
        <div className="columns">
          <div className="column is-half is-offset-one-quarter">
            <div className="field has-addons">
              <div className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  placeholder="dog"
                  onChange={this._onChangeInput}
                  onKeyPress={this._handleKeyPress}
                />
              </div>
              <div className="control">
                <a className="button is-info" onClick={this._buttonClicked}>
                  Random!
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default connect(null, { fetchData })(SearchBar)
