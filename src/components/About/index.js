import React from 'react';

const About = () => {
	return (
		<div>
			<section className="hero is-success is-fullheight">
				<div className="hero-body">
					<div className="container">
						<h1 className="title">
							Page About
      					</h1>
						<h2 className="subtitle">
							Impressioner ?
      					</h2>
					</div>
				</div>
			</section>
		</div>
	);
};

export default About;