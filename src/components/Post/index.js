import React from 'react';


const About = ({ match }) => {
	return (
		<div>
			<section className="hero is-danger is-fullheight">
				<div className="hero-body">
					<div className="container">
						<h1 className="title">
							Page View
      					</h1>
						<h2 className="subtitle">
							ID : #{match.params.id}
      					</h2>
					</div>
				</div>
			</section>
		</div>
	);
};

export default About;