import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
	return (
		<div>
			<nav className="navbar is-black" aria-label="main navigation">
				<div className="navbar-brand">

					<Link className="navbar-item" to="/">
						<img src="https://tctechcrunch2011.files.wordpress.com/2016/02/giphyseriesc.gif?w=100" alt="Giphy Logo" width="100" height="30" />
					</Link>
				</div>
				<div className="navbar-menu">
					<div className="navbar-start">				
						<Link className="navbar-item" to="/">
							Home
						</Link>
						<Link className="navbar-item" to="/about">
							About
						</Link>
						<Link className="navbar-item" to="/post/42">
							Post
						</Link>
					</div>
				</div>
			</nav>
		</div>
	)
}

export default Header