import React, { Component } from 'react'
import Home from "./components/Home";
import About from "./components/About";
import Post from "./components/Post";
import Header from './components/Header'

// Pour react-router-redux
import { history } from "./Store";
import { Route } from 'react-router'
import { ConnectedRouter as Router} from 'react-router-redux'

// CSS Bulma
import 'bulma/css/bulma.css'

class App extends Component {
	render() {
		return (
			<Router history={history} >
				<div>

					<Header />

					<Route exact path="/" component={Home} />
					<Route exact path="/about" component={About} />
					<Route exact path="/post/:id" component={Post} />
					
				</div>
				
			</Router>
		)
  }
}

export default App
