import { combineReducers } from "redux"
import GiphyReducer from "./giphyReducer"

// Routing with Redux
import { routerReducer as routing } from "react-router-redux";

const rootReducer = combineReducers({
    routing,
    giphy: GiphyReducer
})

export default rootReducer