const FETCH_LOADING = 'FETCH_GIPHY::DATA_LOADING'
const FETCH_ERROR = 'FETCH_GIPHY::DATA_ERROR'
const FETCH_SUCCESS = 'FETCH_GIPHY::DATA'

const initialState = {
  loading: true,
  hasError: false,
  tags: 'dog',
  data: {}
}

export default (state = initialState, action = {}) => {

  const { type, payload } = action

  switch (type) {
    case FETCH_LOADING:
    case FETCH_SUCCESS:
    case FETCH_ERROR:
      return Object.assign({}, state, { ...payload })

    default:
      return state
  }
}
