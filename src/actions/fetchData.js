import 'whatwg-fetch'

const FETCH_LOADING = 'FETCH_GIPHY::DATA_LOADING'
const FETCH_ERROR = 'FETCH_GIPHY::DATA_ERROR'
const FETCH_SUCCESS = 'FETCH_GIPHY::DATA'

const fetchDataLoading = (bool) => {
  return {
    type: FETCH_LOADING,
    payload: {
      loading: bool,
      hasError: false,
      data: {}
    }
  }
}

const fetchDataSuccess = (data) => {
  return {
    type: FETCH_SUCCESS,
    payload: {
      loading: false,
      hasError: false,
      data
    }
  }
}

const fetchDataError = (bool) => {
  return {
    type: FETCH_ERROR,
    payload: {
      loading: false,
      hasError: bool,
      data: {}
    }
  }
}

// Action Creator
export const fetchData = (tags) => {
  return async (dispatch) => {
    dispatch(fetchDataLoading(true))

    const API_KEY = 'oHvrulSMjSSOJslU6m76msCmBf7LjELV'
    const TAGS = tags
    const END_POINT = `http://api.giphy.com/v1/gifs/random?api_key=${API_KEY}&tag=${TAGS}`

    console.log(END_POINT)

    try {
      const response = await fetch(END_POINT)
      const json = await response.json()
      //   console.log('JSON', json)
      dispatch(fetchDataSuccess(json.data))
    } catch (error) {
      console.log('Error', error)
      dispatch(fetchDataError(true))
    }
  }
}